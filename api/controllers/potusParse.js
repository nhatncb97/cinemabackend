const rp = require('request-promise');
const $ = require('cheerio');

const potusParse =  (url)=> {
    console.log(url);
    return rp(url)
        .then((html)  =>{
            return {
                date: $(' .film-details-wrap > .film-item > .film-item-txt > .film-overview > span',html).text()
                .split("Thể loại")[0].split("Từ ")[1],
                image: $('.film-details-wrap > .film-item > .film-item-pic > img',html)[0].attribs.src,
                title:$('.film-details-wrap > .film-item > .film-item-pic > img',html)[0].attribs.alt,
                category:$(' .film-details-wrap > .film-item > .film-item-txt > .film-overview > span',html).text()
                .split("Thể loại:")[1].split("Diễn viên:")[0],
                cast:$(' .film-details-wrap > .film-item > .film-item-txt > .film-overview > span',html).text()
                .split("Diễn viên:")[1].split("Đạo diễn:")[0],
                director:$(' .film-details-wrap > .film-item > .film-item-txt > .film-overview > span',html).text()
                .split("Đạo diễn:")[1].split("Đánh giá:")[0],
                sum:$(' .film-details-wrap > .film-item > .film-item-txt > p',html).text()
            };
        })
        .catch( (err)=> {
            console.log('Lỗi')
            //handle error
        });
};

module.exports = potusParse;